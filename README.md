# Launch
1. Open config.txt in project folder and set ip address with port(default "http://localhost:1234")
2. Start app
3. If you see "Started listening!" in console, application started correct

# Requests

Fetches transactions history:
```C
/transactionhistory/ //GET
```
Response:
```json
[  
{  
"id":"f78e2960-1a16-4b14-8854-6d7d8a0fea4d",
"effectiveDate":"2019-02-11T01:33:30.200992+02:00",
"type":"debit",
"amount":99.0
},
{  
"id":"a6258b4e-66ae-4dc9-b5fa-fcef8a49d069",
"effectiveDate":"2019-02-11T01:33:40.771441+02:00",
"type":"debit",
"amount":150.0
},
{  
"id":"6b8c8fa5-dc83-4dbe-8356-8734d727cbf9",
"effectiveDate":"2019-02-11T01:40:39.072764+02:00",
"type":"credit",
"amount":50.0
}
]
```
____
Commit new transaction to the account
```C
/addtransaction/ //POST
```
Parameters:

| Name         | Value                             |
| -------------|-----------------------------------|
| Body(string) | {  "type":"credit", "amount":500} |

Response:
```C
Transaction stored
```
```C
Error:422 Unprocessable Entity
```
____
Find transaction by ID:
```C
/findtransaction/ //GET
```
Parameters:

| Name                  | Value                                |
| ----------------------|--------------------------------------|
| transactionId(string) | 02dace81-ebed-49cf-ab39-0e2ef715d99e |

Response:
```C
Transaction not found!
```
```C
Invalid ID supplied!
```
```json
{  
"id":"f78e2960-1a16-4b14-8854-6d7d8a0fea4d",
"effectiveDate":"2019-02-11T01:33:30.200992+02:00",
"type":"debit",
"amount":99.0
}
```
___
Fetches current account balance:
```C
/currentbalance/ //GET
```
Response:
```json
199
```

