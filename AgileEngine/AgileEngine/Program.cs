﻿using System;
using System.IO;
using AgileEngine.Controllers;

namespace AgileEngine
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            if (!File.Exists("config.txt"))
                File.WriteAllText("config.txt", "http://localhost:1234");

            var address = File.ReadAllText("config.txt");

            var requestsController = new TransactionsRequestsController(address);
            requestsController.Listen();
            Console.ReadKey();
        }
    }
}
