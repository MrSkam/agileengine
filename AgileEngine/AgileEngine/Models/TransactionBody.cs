﻿using System;

namespace AgileEngine.Models
{
    public class TransactionBody
    {
        public string type;
        public float amount;

        public TransactionBody(string type, float amount)
        {
            this.type = type;
            this.amount = amount;
        }
    }

}