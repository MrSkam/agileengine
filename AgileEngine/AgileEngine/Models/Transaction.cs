﻿using System;
namespace AgileEngine.Models
{
    public class Transaction : TransactionBody
    {
        public string id;
        public DateTime effectiveDate;

        public Transaction(string id, string type, float amount, DateTime effectiveDate) : base(type, amount)
        {
            this.id = id;
            this.effectiveDate = effectiveDate;
        }
    }
}
