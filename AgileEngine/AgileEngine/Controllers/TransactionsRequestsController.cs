﻿using System;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using Newtonsoft.Json;
using AgileEngine.Models;

namespace AgileEngine.Controllers
{
    public delegate void RequestDelegate(HttpListenerContext context);

    public class TransactionsRequestsController
    {
        private HttpListener _listener;
        private Dictionary<string, RequestDelegate> _getRequests;
        private Dictionary<string, RequestDelegate> _postRequests;

        private TransactionsController _transactionsController;
        private object _locker = new object();

        public TransactionsRequestsController(string address)
        {
            _listener = new HttpListener();

            _getRequests = new Dictionary<string, RequestDelegate>();
            _postRequests = new Dictionary<string, RequestDelegate>();

            _transactionsController = new TransactionsController();

            //Get
            _listener.Prefixes.Add(address + "/transactionhistory/");
            _getRequests.Add("/transactionhistory/", OnGetTransactionsHistory);

            _listener.Prefixes.Add(address + "/findtransaction/");
            _getRequests.Add("/findtransaction/", OnFindTransactionByID);

            _listener.Prefixes.Add(address + "/currentbalance/");
            _getRequests.Add("/currentbalance/", OnGetBalance);

            //Post
            _listener.Prefixes.Add(address + "/addtransaction/");
            _postRequests.Add("/addtransaction/", OnAddTransaction);
        }

        public async Task Listen()
        {
            _listener.Start();
            Console.WriteLine("Started listening!");
            while (true)
            {
                HttpListenerContext context = await _listener.GetContextAsync();
                HttpListenerRequest request = context.Request;

                RequestDelegate requestDelegate;
                switch (request.HttpMethod)
                {
                    case "GET":
                        if (_getRequests.TryGetValue(request.Url.AbsolutePath, out requestDelegate))
                            requestDelegate(context);
                        else
                        {
                            var response = context.Response;
                            SendResponse(response, "Invalid method type! Use POST");
                        }
                        break;
                    case "POST":
                        if (_postRequests.TryGetValue(request.Url.AbsolutePath, out requestDelegate))
                            requestDelegate(context);
                        else
                        {
                            var response = context.Response;
                            SendResponse(response, "Invalid method type! Use GET");
                        }
                        break;
                }
            }
        }

        private void OnGetTransactionsHistory(HttpListenerContext context)
        {
            var json = JsonConvert.SerializeObject(_transactionsController.transactions);
            var response = context.Response;
            var request = context.Request;

            SendResponse(response, json);
        }

        private void OnFindTransactionByID(HttpListenerContext context)
        {
            var request = context.Request;
            var response = context.Response;

            var transactionID = request.QueryString["transactionId"];

            var tempGuid = new Guid();
            if (String.IsNullOrEmpty(transactionID) || !Guid.TryParse(transactionID, out tempGuid))
            {
                SendResponse(response, "Invalid ID supplied!");
                return;
            }

            var transaction = _transactionsController.GetTransactionByID(transactionID);

            if (transaction != null)
            {
                var json = JsonConvert.SerializeObject(transaction);
                SendResponse(response, json);
            }
            else
            {
                Console.WriteLine(transactionID);
                SendResponse(response, "Transaction not found!");
            }
        }

        private void OnGetBalance(HttpListenerContext context)
        {
            var balance = _transactionsController.GetBalance().ToString();
            var response = context.Response;
            var request = context.Request;

            SendResponse(response, balance);
        }

        private void OnAddTransaction(HttpListenerContext context)
        {

            var requset = context.Request;
            var response = context.Response;
            var request = context.Request;

            var bodyJson = requset.QueryString["body"];

            try
            {
                var body = JsonConvert.DeserializeObject<TransactionBody>(bodyJson);

                bool correctTransaction = true;
                lock (_locker)
                {
                    correctTransaction = _transactionsController.TryAddTransaction(body.type, body.amount, DateTime.Now);
                }
                if(correctTransaction)
                    SendResponse(response, "Transaction stored");
                else
                    SendResponse(response, "Error:422 Unprocessable Entity");
            }
            catch
            {
                SendResponse(response, "Invalid input!");
            }
        }

        private void SendResponse(HttpListenerResponse response, string value)
        {
            response.Headers.Add("Access-Control-Allow-Credentials", "true");
            response.Headers.Add("Access-Control-Allow-Headers", "Accept, X-Access-Token, X-Application-Name, X-Request-Sent-Time");
            response.Headers.Add("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
            response.Headers.Add("Access-Control-Allow-Origin", "*");

            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(value);
            response.ContentLength64 = buffer.Length;

            Stream output = response.OutputStream;

            output.Write(buffer, 0, buffer.Length);
            output.Close();
        }
    }
}