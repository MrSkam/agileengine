﻿using System;
using System.Collections.Generic;
using AgileEngine.Models;

namespace AgileEngine.Controllers
{
    public class TransactionsController
    {
        public List<Transaction> transactions
        {
            get
            {
                return _transactions;
            }
        }

        private List<Transaction> _transactions;

        public TransactionsController()
        {
            _transactions = new List<Transaction>();
        }

        public void AddTransaction(string type, float amount, DateTime effectiveDate)
        {
            var temp = new Transaction(Guid.NewGuid().ToString(), type, amount, effectiveDate);
            _transactions.Add(temp);
        }

        public bool TryAddTransaction(string type, float amount, DateTime effectiveDate)
        {
            if (type.ToLower() == "credit")
            {
                if (amount > GetBalance())
                    return false;
            }
            AddTransaction(type, amount, effectiveDate);
            return true;
        }
    

        public float GetCreditsAmount()
        {
            var credits = _transactions.FindAll(pred => pred.type == "credit");
            var sum = 0f;

            for (var creditIndex = 0; creditIndex < credits.Count; creditIndex++)
                sum += credits[creditIndex].amount;

            return sum;
        }

        public float GetDebitsAmount()
        {
            var debits = _transactions.FindAll(pred => pred.type == "debit");

            var sum = 0f;

            for (var debitIndex = 0; debitIndex < debits.Count; debitIndex++)
                sum += debits[debitIndex].amount;

            return sum;
        }

        public float GetBalance()
        {
            var debit = GetDebitsAmount();
            var credit = GetCreditsAmount();
            return debit - credit;
        }

        public Transaction GetTransactionByID(string id)
        {
            return _transactions.Find(pred => pred.id == id);
        }
    }
}